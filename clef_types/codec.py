# Functions to encode/decode Clef packets.

import cbor2

from typing import Any, Dict, Tuple

from clef_types.constants import CLEF_MAGIC, PROTOCOL_MARKER, PacketOpcode
from clef_types.exceptions import IncompletePacketError, InvalidEncodedValueError, InvalidMagicNumberError, InvalidOpcodeError, InvalidValueError, PacketDecodeError, PacketEncodeError, TypeSerializationError
from clef_types.types import ClefPacket


def encode_packet(
    *,
    opcode: PacketOpcode = None,
    data: Any = None,
    options: Dict[Any, Any] = None,
    continuation: int = 0
) -> bytes:
    """Encode the given data into a Clef packet.

    This function takes the payload and options as separate parameters, and also requires
    the opcode to be stated explicitly.
    """

    # We have to do some work to handle the fixed-width values for content length and continuation.
    # This requires us to build our own CBOR integers. A 4-byte unsigned integer is always prefixed
    # with the byte 0x1a (0b0001_1010) for major type 0, additional information 26).
    PREFIX_BYTE = b'\x1a'

    if opcode is None:
        raise PacketEncodeError("An opcode must be provided")

    try:
        # CBOR-encode the options and payload sections.
        options_section = cbor2.dumps(options if options is not None else {})
        payload_section = cbor2.dumps(data)

        # Calculate the length of the packet's content. This is options + payload, not counting
        # the fixed 16 bytes of the header.
        content_length = len(options_section) + len(payload_section)

        # Create the header. We can't do this using the CBOR library itself, but it's a pretty simple
        # header to construct manually.
        header = b"".join(
            CLEF_MAGIC.to_bytes(),
            opcode.to_bytes(),
            PROTOCOL_MARKER.to_bytes(),
            PREFIX_BYTE,
            content_length.to_bytes(4, "big"),
            PREFIX_BYTE,
            continuation.to_bytes(4, "big")
        )

        message = b"".join(header, options_section, payload_section)
        return message
    except cbor2.CBOREncodeTypeError as exc:
        raise TypeSerializationError(str(exc)) from exc
    except cbor2.CBOREncodeValueError as exc:
        raise InvalidValueError(str(exc)) from exc
    except cbor2.CBOREncodeError as exc:
        raise PacketEncodeError(str(exc)) from exc
    except Exception as exc:
        raise PacketEncodeError(str(exc)) from exc


def decode_packet(message: bytes) -> ClefPacket:
    """Decode a Clef packet.
    
    The return value is a custom object with properties for all relevant data within the packet.
    """

    # Some sanity checks.
    if len(message) < 18:
        # A packet must be no less than 18 bytes in length: 16 for the header, 1 each for options and payload.
        raise IncompletePacketError("Packet must be at least 18 bytes")

    magic = int.from_bytes(message[:4], "big")
    if magic != CLEF_MAGIC:
        # Magic number is incorrect, so this isn't actually a Clef packet.
        raise InvalidMagicNumberError(f"Magic number is {hex(magic)} instead of {hex(CLEF_MAGIC)}")
    
    try:
        # Check that the opcode byte is in the correct range, and isn't a reserved value.
        opcode_byte = int.from_bytes(message[5])
        if opcode_byte < 0x60 or opcode_byte > 0x6f:
            raise InvalidOpcodeError(f"{hex(opcode_byte)} is not a valid opcode")
            
        opcode = PacketOpcode(opcode_byte)
    except ValueError as exc:
        raise InvalidOpcodeError(f"Opcode {hex(opcode_byte)} is reserved")

    try:
        # Now try decoding the message as a CBOR object.
        decoded_message = cbor2.loads(message)

        # Sanity check that the opcode is the same as the lowest byte of the tag.
        assert opcode == (decoded_message.tag & 0xff)

        length, continuation, options, payload = decoded_message.value

        # Verify the content length is correct.
        if len(message) != length + 16:
            raise IncompletePacketError(f"Invalid content length: expected {length}, got {len(message)}")

        # Basically a switch on all known opcode types, so we get the correct class.
        if opcode == PacketOpcode.REQUEST:
            pass
        elif opcode == PacketOpcode.RESPONSE:
            pass
        elif opcode == PacketOpcode.DISCOVERY_QUERY:
            pass
        elif opcode == PacketOpcode.DISCOVERY_RESPONSE:
            pass
        elif opcode == PacketOpcode.TLS_NEGOTIATION:
            pass
        elif opcode == PacketOpcode.TLS_RESPONSE:
            pass
        elif opcode == PacketOpcode.AUTHENTICATE:
            pass
        elif opcode == PacketOpcode.AUTHENTICATE_RESPONSE:
            pass
        elif opcode == PacketOpcode.BATCH:
            pass
        elif opcode == PacketOpcode.ERROR_RESPONSE:
            pass
        elif opcode == PacketOpcode.CONTINUE:
            pass
        elif opcode == PacketOpcode.ACKNOWLEDGE:
            pass
        elif opcode == PacketOpcode.DISCONNECT:
            pass
        else:
            # Shouldn't happen because of the sanity checks earlier.
            raise InvalidOpcodeError(f"Reserved opcode {hex(opcode)}")

    except cbor2.CBORDecodeValueError as exc:
        raise InvalidEncodedValueError(str(exc)) from exc
    except cbor2.CBORDecodeEOF as exc:
        raise IncompletePacketError("EOF encountered")
    except cbor2.CBORDecodeError as exc:
        raise PacketDecodeError(str(exc)) from exc
    except Exception as exc:
        raise PacketDecodeError(str(exc)) from exc
