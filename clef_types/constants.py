# Constants and enums for Clef

from enum import IntEnum

# The magic number for a Clef packet. This is found at the start of the header, and it serves two purposes.
# First, it identifies the data structure as a Clef packet. Second, it combines with the opcode to create
# a custom CBOR tag for each packet type.
CLEF_MAGIC = 0xda704177

# The default port reserved for a Clef metaserver.
METASERVER_PORT = 3737

# The protocol marker, used in a Clef header.
PROTOCOL_MARKER = 0x84

class PacketOpcode(IntEnum):
    """The byte representing the opcode of a Clef packet.
    
    This is always in the range 0x60-0x6f. Only the last 4 bits are used, but the 0x60 offset
    is necessary to preserve the CBOR tag number for the opcode.
    """

    REQUEST = 0x60
    RESPONSE = 0x61
    DISCOVERY_QUERY = 0x62
    DISCOVERY_RESPONSE = 0x63
    TLS_NEGOTIATION = 0x64
    TLS_RESPONSE = 0x65
    AUTHENTICATE = 0x66
    AUTHENTICATE_RESPONSE = 0x67
    BATCH = 0x68
    ERROR_RESPONSE = 0x69
    CONTINUE = 0x6c
    ACKNOWLEDGE = 0X6e
    DISCONNECT = 0x6f

    @classmethod
    def from_tag(cls, tag: int):
        return cls(tag & 0xff)

    def to_tag(self) -> int:
        return (CLEF_MAGIC << 8) + self.value

class ProtocolOption(IntEnum):
    """The protocol-level options for Clef. All others are currently reserved by the specification."""

    PROTOCOL_VERSION = 0x01
    AUTHORIZATION = 0x05
    TIMESTAMP = 0x08
    EXPIRES = 0x09
    SIGNATURE = 0x0d
    SIGNING_ALGORITHM = 0x0e
    PING = 0x10


class StandardOption(IntEnum):
    """Standard options for Clef packets. These are common to all packet types, and are defined by the spec itself."""

    SERVICE_NAME = 0x20
    SERVICE_DISCOVERY = 0x40
    SERVICE_DESCRIPTION = 0x41
    USERNAME = 0x50
    PASSWORD = 0x51
    AUTHENTICATION_TOKEN = 0x52


class DiscoveryTypeCode(IntEnum):
    """Standardized type codes for discovery responses."""

    UINT8 = 0x00
    UINT16 = 0x01
    UINT32 = 0x02
    UINT64 = 0x63
    INT8 = 0x04
    INT16 = 0x05
    INT32 = 0x06
    INT64 = 0x07
    FLOAT16 = 0x80
    FLOAT32 = 0x09
    FLOAT64 = 0x0a
    BOOLEAN = 0x0c
    STRING = 0x10
    POINTER = 0x11
    ARRAY = 0x12
    MAP = 0x13
    BINARY_DATA = 0x14


class ErrorCode(IntEnum):
    """Standardized error response codes. This does not include the "private use" codes in the range 0x40-0x5a."""

    VALIDATION_FAILURE = 0x22
    PAYMENT_REQUIRED = 0x24
    EXPIRED = 0x2e
    BAD_REQUEST = 0x2f
    NO_SERVICE_MATCH = 0x37
    UNAUTHORIZED = 0x38
    INCORRECT_CREDENTIALS = 0x39
    FORBIDDEN = 0x3a
    CONFLICT = 0x3d
    NOT_IMPLEMENTED = 0x3f
    ENCRYPTION_REQUIRED = 0x5f
    SERVER_ERROR = 0x70
    RATE_LIMIT_EXCEEDED = 0x72
    INSUFFICIENT_RESOURCES = 0x73
    UNSUPPORTED_PROTOCOL = 0x7c
    INVALID_OPERATION = 0x7f
